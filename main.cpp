using namespace std;
#include <bits/stdc++.h>
#include <ostream>
#include <stdio.h>

struct node{
    char character;
    int char_frequency=0;
    node *l_child=nullptr, *r_child=nullptr;

    node() {}
    node(char character, int number_of_repetitions) :
    character(character), char_frequency(number_of_repetitions) {}

    int getPriority(){
        return char_frequency;
    }

    char getName() const {
        return character;
    }

    void setL_child(node *l_child) {
        node::l_child = l_child;
    }

    void setR_child(node *r_child) {
        node::r_child = r_child;
    }

    node *getL_child() const {
        return l_child;
    }

    node *getR_child() const {
        return r_child;
    }

    bool operator<(const node &rhs) const {
        return char_frequency < rhs.char_frequency;
    }

    bool operator>(const node &rhs) const {
        return rhs < *this;
    }

    bool operator<=(const node &rhs) const {
        return !(rhs < *this);
    }

    bool operator>=(const node &rhs) const {
        return !(*this < rhs);
    }

    friend ostream &operator<<(ostream &os, const node &node1) {
        os << "character: " << node1.character << " char_frequency: " << node1.char_frequency << " l_child: "
           << node1.l_child << " r_child: " << node1.r_child;
        return os;
    }
};

template <class t>
struct BHeap{
    vector<t> heap;

    BHeap(){
        t v;
        heap.push_back(v);
    }
    void heapify(int ind){
        if(ind != 1){
            while (ind>1 && heap[ind]<heap[ind/2]){
                swap(heap[ind], heap[ind/2]);
                ind /= 2;
            }
        } else{
            while (heap.size() > 2 * ind ){
                int largest = ind;      // Initialize largest as root
                int l = 2 * ind;        // left = 2*i
                int r = 2 * ind + 1;    // right = 2*i + 1
                if(heap.size() > r && heap[l] > heap[r] && heap[largest] > heap[r]){
                    swap(heap[ind], heap[r]);
                    ind = r;
                }else if( (heap.size() <= r || heap[l] <= heap[r]) && heap[largest] > heap[l]){
                    swap(heap[ind], heap[l]);
                    ind = l;
                } else break;
            }
        }
    }

    void insert(t v){
        heap.push_back(v);
        heapify(int(heap.size() - 1));
    }

    t remove(){
        t v = heap[1];
        swap(heap[heap.size()-1], heap[1]);
        heap.pop_back();
        heapify(1);
        return v;
    }

    int size(){
        return int(heap.size());
    }

};

vector<string> read_input_file(map<char, int> &char_counter){
    vector<string> fileData;
    string fileAddress, line;
    cout << "enter input file address: ";
    cin >> fileAddress;
    ifstream myFile (fileAddress);
    if (myFile.is_open())
    {
        char_counter[0] = 0;
        while ( getline (myFile,line) )
        {

            char_counter['\n']++;
            for (auto c : line){
                char_counter[c]++;
            }
            line += '\n';
            fileData.push_back(line);
        }
        line = line.substr(0, line.size()-1);
        fileData[fileData.size()-1] = line;
        myFile.close();
    }
    return fileData;
}

node* huffman_tree(map<char, int> char_counter){
    BHeap<node> bHeap;
    for (auto const& x : char_counter)
    {
        bHeap.insert(node(x.first, x.second));
    }

    node *aPtr, *bPtr, *cPtr;
    while (bHeap.size()>2){
        aPtr = new node(bHeap.remove());
        bPtr = new node(bHeap.remove());
        cPtr = new node(char(254), aPtr->getPriority() + bPtr->getPriority());
        cPtr->setL_child(aPtr);
        cPtr->setR_child(bPtr);

        bHeap.insert(*cPtr);
    }
    return new node(bHeap.remove());
}

void huffman_code_dfs(node* v, string code, map<char, string> &huffman_map){
    if (v->getL_child() == nullptr){
        huffman_map[v->getName()] = code;
        return;
    }
    huffman_code_dfs(v->getL_child(), code+"0", huffman_map);
    huffman_code_dfs(v->getR_child(), code+"1", huffman_map);
}

string binaryToAscii(string compressed_data){
    stringstream sstream(compressed_data);
    string output;
    while(sstream.good())
    {
        std::bitset<8> bits;
        sstream >> bits;
        char c = char(bits.to_ulong());
        output += c;
    }
    return output;
}

void write_huffman_code_file(map<char, string> huffman_map){
    string fileAddress, line;
    cout << "enter output huffmaan code file address: ";
    cin >> fileAddress;
    ofstream myFile (fileAddress);
    if (myFile.is_open())
    {
        for (auto const& x : huffman_map)
        {
            myFile << x.first << '\t' << x.second.length() << '\t' << x.second << endl;
        }
        myFile.close();
    }
}

void write_huffman_compress_file(map<char, string> huffman_map, vector<string> inpFile){
    string fileAddress;
    cout << "enter output compress file address: ";
    cin >> fileAddress;
    ofstream myFile(fileAddress, ios::binary);
    string compressed_data;
    for(auto str: inpFile){
        for(auto c: str){
            compressed_data += huffman_map[c];
        }
    }
    compressed_data += huffman_map[0];
    if(compressed_data.length()%8 != 0){
        compressed_data.insert(compressed_data.length(), 8-(compressed_data.length()%8), '0');
    }
//    cout << compressed_data << endl;

    compressed_data = binaryToAscii(compressed_data);
//    cout << compressed_data << endl;

    if (myFile.is_open()){
        myFile << compressed_data;
        myFile.close();
    }
}

void huffman_encode(){
    map<char, int> char_counter;
    map <char, string> huffman_map;
    vector<string> file_data;

    file_data = read_input_file(char_counter);

    node *v = huffman_tree(char_counter);

    huffman_code_dfs(v, "", huffman_map);

    write_huffman_code_file(huffman_map);

    write_huffman_compress_file(huffman_map, file_data);

}

void read_huffman_code_file(map<string, char> &huffman_map){
    string fileAddress, line1, line2, line3, line4;
    cout << "enter input huffman code file address: ";
    cin >> fileAddress;

    ifstream myFile(fileAddress);
    if (myFile.is_open())
    {
        char t;
        while (myFile.get(t) && getline(myFile, line2, '\t') &&
        getline(myFile, line3, '\t') &&
        getline(myFile, line4, '\n')){
            line1 = t;
            huffman_map[line4] = line1[0];
        }
        myFile.close();
    }
}

string read_compressed_file(){
    string fileAddress, line;
    cout << "enter input compressed file address: ";
    cin >> fileAddress;

    char *buffer = new char[1];

    ifstream myFile(fileAddress, ios::binary);
    if (myFile.is_open())
    {
        while (!myFile.eof()){
            myFile.read(buffer, 1);
            bitset<8> bits(static_cast<unsigned long long int>(buffer[0]));
            line += bits.to_string();
        }
        myFile.close();
    }
    return line;
}

string deCompress_string(map<string, char> huffman_map, string compressed_data){
    int i=0,j=1;
    string decompressed_data = "";
    while (compressed_data.length()>i){
        string temp = compressed_data.substr(i, j);
        if(huffman_map.find(temp) != huffman_map.end()){
            if(huffman_map[temp]==0)break;
            decompressed_data += huffman_map[temp];
            i += j;
            j=1;
        } else{
            j++;
        }
    }
    return decompressed_data;
}

void write_deCompress_data(string data){
    string fileAddress;
    cout << "enter address for decompress data: ";
    cin >> fileAddress;

    ofstream myFile(fileAddress);

    if(myFile.is_open()){
        myFile << data;
        myFile.close();
    }

}

void huffman_decode(){
    map<string, char> huffman_map;
    read_huffman_code_file(huffman_map);
    string compressed_data = read_compressed_file();
    string decompressed_data = deCompress_string(huffman_map, compressed_data);
    write_deCompress_data(decompressed_data);
}

int main() {
    int i=1;
    while (true){
        cout << "*******************************************\n"
        << "huffman encode encode: 1\n" << "huffman decode: 2\nenter a number: " << endl;
        cin >> i;
        if(i==0)break;
        if(i==1)huffman_encode();
        if(i==2)huffman_decode();
    }
    return 0;
}